## Formula One championships
This project was bootstrapped with create-react-app. As the assignment doesn't set any restrictions on the environment, I didn't use any polyfill for backwards compatibility.

### Project structure
The project has been split into screens and components. In the current project, I aimed to create stateless components so that they can be easily reused in any part of the application. Driver component has been deliberately combined with DriverList as these two components are used together but can be easily separated if needed.

### DataAdapter and Transport
The purpose of DataAdapter is to create a unified way of working with data. The idea is that a developer should be able to focus on the data itself not on where the data is stored and how to retrieve the data. It also allows to easily change sources of data. If, for example, later we decide to store "favourites" on the backend it will only require minor changes in the implementation of the "/favorites/" interface. As an example of different sources of data, I implemented localStorage ans jsonp transports.
A transport instance must implement window.fetch interface.

### UI
I implemented minimalistic fluid UI. Though, the result looks good on small screens mobile experience can be further improved.
Asa css "framework" I used BEM (http://getbem.com/).

### Improvements
Due to limited time, I couldn't implement all the ideas that I had. Here goes the list of improvements:
1. Add tests. The application has been written with tests in mind and has a modular structure.
2. I wanted to implement caching functionality for DataAdapter.
3. Implement a simplified format for URIPattern. The current implementation expects URIPattern to be a valid regexp,
   a simplified format like "/seasons/:season" would be much easier to use.
4. Improve mobile experience
5. Use SVG where possible
6. Implement Google or Bing Search Image API to show real photos of drivers
7. Current implementation doesn't have any pagination
