import Transport from '../Transport/Transport';

class DataAdapter {
    _fallbackAdapter = {URIRegExp: null, URIPattern: null, transport: Transport.fetch};
    _adapters = [];

     _findSource(uri) {
         let source = this._adapters.find((s) =>  {
             return s.URIRegExp.test(uri) && s
         });

         if (!source) {
            console.warn(`No adapter was found for ${uri}, falling back to default`);
            source = this._fallbackAdapter;
         }

         return source;
     }
    /**
     * Registers end-point for given URI or a URI pattern
     *
     * @param {string} URIPattern Pattern for the URI, must be a valid regexp
     *
     * @param {object} options Options of the DataAdapter
     *
     * @param {function} [options.transport] A function that resolves communication with data source
     *      implements window.fetch interface
     *      @returns {Promise}
     *
     * @param {string} [options.map] A URIPattern is mapped against this value. Must be a valid regexp
     * 
     * @param {object} [options.defReqOptions] Request default options
     *
     * @param {function} [options.preprocessor] Processes data before returning it to the consumer
     *      @returns {Promise}
     *
     * @returns undefined
     */
    register(URIPattern, options) {
        let URIRegExp;

        if (!options.transport || typeof options.transport !== 'function') {
            console.error(`DataManager: transport must be a function`);
        }

        try {
            URIRegExp = new RegExp(`^${URIPattern}$`);
        } catch (e) {
            console.error(`DataManager: wrong URI pattern ${URIPattern}, must be valid RegExp`);
            throw e;
        }

        this._adapters.push({URIRegExp, ...options})
    }

  /**
   * Executes a request using one of the registered DataAdapters
   * defaults to a standard window.fetch if none of the register DataAdapters matched the URI
   * 
   * @param {string} uri The URI of the data source
   * 
   * @param {object} [reqOptions]  Request options (see window.fetch documentation)
   * 
   * @param {string} [reqOptions.method="GET"]  Method of the request (e.g "POST", "GET")
   * 
   * @param {object} [reqOptions.body]  Payload of the request
   * 
   * @returns {Promise}
   */
    fetch(uri, reqOptions) {
        const source = this._findSource(uri),
            { preprocessor, map, defReqOptions } = source;

        if (map) {
            uri = uri.replace(source.URIRegExp, map);
        }

        return source.transport(uri, Object.assign({}, defReqOptions, reqOptions)).then((response) => {
            return (typeof preprocessor === 'function' && preprocessor(response)) || response;
        });
    }
}

export default new DataAdapter()