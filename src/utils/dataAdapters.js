import DataAdapter from './DataAdapter/DataAdapter';
import Transport from './Transport/Transport';

function DriverTablePreprocessor (response) {
    return response.json().then((json) => {
        try {
            return json.MRData.DriverTable.Drivers
        } catch (e) {
            return Promise.reject(e)
        }
    });
}

DataAdapter.register('/season/([0-9]{4})/drivers/?', {
    map: 'http://ergast.com/api/f1/$1/drivers.json',
    transport: Transport.fetch,
    defReqOptions: {mode: 'cors'},
    preprocessor: DriverTablePreprocessor
});

DataAdapter.register('/season/([0-9]{4})/winners/?', {
    map: 'http://ergast.com/api/f1/$1/results/1/drivers.json',
    transport: Transport.fetch,
    defReqOptions: {mode: 'cors'},
    preprocessor: DriverTablePreprocessor
});

DataAdapter.register('/favorites/?', {
    transport: Transport.localStorage,
    defReqOptions: {method: 'GET'},
    preprocessor: (response) => {
        let json = {};
        if (response.ok) {
            json = response.data;
        }

        return json;
    }
});

