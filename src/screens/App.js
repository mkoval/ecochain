import React, {Component} from 'react';
import {Route} from 'react-router-dom';

import SeasonList from '../components/SeasonList/SeasonList';
import SeasonDrivers from '../screens/SeasonDrivers/SeasonDrivers';
import FavoriteDrivers from '../screens/FavoriteDrivers/FavoriteDrivers';
import Nav from '../components/Nav/Nav';

import './App.css';

class App extends Component {
  render() {
    return (
      <div className="app">
        <Route exact path="/"
            render={()=> (
              <React.Fragment>
                <Nav title="Season list" />
                <SeasonList start={2009} end={2018} />
              </React.Fragment>
            )}
        />

        <Route path="/season/:season"
            render={({match})=>(
              <React.Fragment>
                <Nav title={`${match.params.season} season driver list`} />
                <SeasonDrivers season={match.params.season} />
              </React.Fragment>
            )}
        />

        <Route path="/favorites"
            render={()=>(
              <React.Fragment>
                <Nav title='Favorite list' />
                <FavoriteDrivers />
              </React.Fragment>
            )}
        />
      </div>
    );
  }
}

export default App;
