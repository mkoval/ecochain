import React, { Component } from 'react';
import DriversList from '../../components/DriversList/DriversList';

import DataAdapter from '../../utils/DataAdapter/DataAdapter';


class SeasonDrivers extends Component {
  constructor(props) {
    super();

    this.state = {
      season: props.season,
      drivers: [],
      loading: true
    }
  }

  favorites = [];

  componentDidMount = () => {
    const seasonDrivers = DataAdapter.fetch(`/season/${this.state.season}/drivers/`),
      seasonWinners = DataAdapter.fetch(`/season/${this.state.season}/winners/`),
      favorites = DataAdapter.fetch(`/favorites/`);

    Promise.all([seasonDrivers, favorites, seasonWinners]).then((values) => {
      let [seasonDrivers, favorites, seasonWinners] = values;

      seasonDrivers.forEach((driver) => {
        driver.isWinner = seasonWinners.some((winner) => {return driver.driverId === winner.driverId});
        driver.isFavorite = favorites.some((favorite) => {return driver.driverId === favorite.driverId});
      });

      this.favorites = favorites;
      this.setState({drivers: seasonDrivers, loading: false});
    });
  };

  onFavoriteClick = (driver) => {
    const {isFavorite, driverId} = driver,
      seasonDrivers = this.state.drivers;

    if (isFavorite) {
      this.favorites = this.favorites.filter((favorite) => {return favorite.driverId !== driverId});
    } else {
      driver.isFavorite = true;
      this.favorites.push(driver);
    }


    DataAdapter
      .fetch('/favorites/', {method: 'POST', body: this.favorites})
      .then(()=> {
        seasonDrivers.forEach((seasonDriver) => {
          if (seasonDriver.driverId === driverId) {
            seasonDriver.isFavorite = !isFavorite;
          }
        });

        this.setState({drivers: seasonDrivers});
      })
  };

  render() {
    const { drivers, loading } = this.state;

    return (
      <div className="season-drivers" data-loading={loading}>
        {drivers.length ? <DriversList onFavoriteClick={this.onFavoriteClick} drivers={drivers} /> : <h2>We couldn't find any drivers</h2>}
      </div>
    );
  }
}

export default SeasonDrivers;
