import React, { Component } from 'react';
import DriversList from '../../components/DriversList/DriversList';

import DataAdapter from '../../utils/DataAdapter/DataAdapter';

class FavoriteDrivers extends Component {
  constructor(props) {
    super();

    this.state = {
      drivers: [],
      loading: true
    }
  }

  componentDidMount = () => {
    DataAdapter.fetch(`/favorites/`).then((favorites) => {
      this.setState({drivers: favorites, loading: false});
    });
  };

  onFavoriteClick = (driver) => {
    const { driverId} = driver;
    let favoriteDrivers = this.state.drivers;

    favoriteDrivers = favoriteDrivers.filter((favorite) => {return favorite.driverId !== driverId});

    DataAdapter
      .fetch('/favorites/', {method: 'POST', body: favoriteDrivers})
      .then((res)=> {
        this.setState({drivers: res});
      })
  };

  render() {
    const {drivers, loading } = this.state;

    return (
      <div className="favorite-drivers" data-loading={loading}>
        {drivers.length ? <DriversList onFavoriteClick={this.onFavoriteClick} drivers={drivers} /> : <h2>We couldn't find any drivers</h2>}
      </div>
    );
  }
}

export default FavoriteDrivers;
