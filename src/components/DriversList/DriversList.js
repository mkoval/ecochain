import React, { Component } from 'react';

import './Driver.css';
import './DriverList.css';

class Driver extends Component {
  render() {
    const {driver, onFavoriteClick} = this.props;

    return (
      <div className="driver">
        {driver.isWinner && <div className="driver--trophy"></div>}
        <div title="Add to favorite list" className={'driver--favorite ' + (driver.isFavorite ? 'driver--favorite__favorite' : '')} data-favorite={driver.favorite} onClick={(e) => {onFavoriteClick(driver)}}>♥</div>
        <div className="driver--photo"></div>
        <div className="driver--info">
          <h4 className="driver--name"><a href={driver.url} target="_blank">{driver.givenName} {driver.familyName}</a></h4>
          <p className="driver--date-of-birth"><strong>Born</strong> <span>{driver.dateOfBirth}</span></p>
          <p className="driver--nationality"><strong>Nationality</strong> <span>{driver.nationality}</span></p>
        </div>
      </div>
    )
  }
}

class Drivers extends Component {
  render() {
    const {drivers, onFavoriteClick} = this.props;

    return (
      <ul className="drivers-list">
        {drivers.map((driver) => {
          return <li key={driver.driverId}><Driver onFavoriteClick={onFavoriteClick} driver={driver} /></li>
        })}
      </ul>
    );
  }
}

export default Drivers;
