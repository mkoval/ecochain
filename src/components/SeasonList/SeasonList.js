import React, { Component } from 'react';
import {Link} from 'react-router-dom';

import './SeasonList.css';

class SeasonList extends Component {
  render() {
    let seasonList;

    const {start, end} = this.props,
      listLength = (end + 1) - start;  //end + 1 because last year included

    if (listLength > 0) {
      seasonList = new Array(listLength).fill(start).map((value, index) => (value + index))
    } else {
      seasonList = [];
      console.warn('SeasonList: season list length <= 0');
    }

    return (
      <ul className="season-list">
        {seasonList.map((season) => {
          return <li key={season}><Link to={"/season/" + season}>{season}</Link></li>
        })}
      </ul>
    );
  }
}

export default SeasonList;
