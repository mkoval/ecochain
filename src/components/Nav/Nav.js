import React, { Component } from 'react';
import {Link} from 'react-router-dom';

import './Nav.css';

class Nav extends Component {
  render() {
    const {title} = this.props;

    return (
      <nav className="app-nav">
        <Link to="/" className="app-nav--btn nav-seasons">Seasons list</Link>
        <h3 className="app-nav--title">{title}</h3>
        <Link to="/favorites" className="app-nav--btn nav-seasons">Favorites</Link>
      </nav>
    );
  }
}

export default Nav;
